﻿using System;
using System.Linq;
using InterfaceLib;
using EFLib;
//using ADOLib;

namespace Avito
{
    class Program
    {
        static void Main()
        {   
            using var repoUser = new RepositoryUser();
            using var repoAdvertisement = new RepositoryAdvertisement();

            Authentication authentication = new Authentication(repoUser);
            Console.Write("Введите логин: ");
            string Login = Console.ReadLine();
            Console.Write("Введите пароль: ");
            string Password = Console.ReadLine();
            var user = authentication.GetUser(Login, Password);
            
            if (user != null)
            {               
                RepositoryQuery repositoryQuery = new RepositoryQuery(user, repoAdvertisement);

                while (repositoryQuery.State)
                {
                    Console.WriteLine("\nКакие действия хотите выполнить:\n 1 - просмотр объявления\n 2 - добавить объявление\n 3 - изменить объявление\n 4 - удалить объявление\n" +
                                      " 5 - вывести все активные объявления\n 6 - вывести объявления дешевле заданной суммы\n 7 - вывести объявления, добавленные после заданной даты\n" +
                                      " 8 - вывести статистику просмотра объявлений\n 9 - вывести наиболее просматриваемые объявления за сутки\n q - выход");

                    Console.WriteLine(repositoryQuery.GetResultQuery(Console.ReadLine()));                    
                }
            }            
        }
    }
}
