﻿using System;
using System.Collections.Generic;
using System.Text;
using InterfaceLib;

namespace Avito
{
    public class Authentication
    {
        private readonly IRepositoryUser repositoryUser;
        public Authentication(IRepositoryUser _repositoryUser)
        {
            repositoryUser = _repositoryUser;
        }

        public User GetUser(string Login,string Password)
        {
            var user = repositoryUser.GetUser(Login, Password);
            if (user != null)
            {
                Console.Write($"Добро пожаловать, {user.ToString()}");                 
            }                
            else
            {
                Console.WriteLine("Ошибка входа пользователя\nРегистрация:\n");
                Console.Write("Введите имя: ");
                string Name = Console.ReadLine();
                Console.Write("Введите фамилию: ");
                string LastName = Console.ReadLine();
                Console.WriteLine("Введите Email: ");
                string Email = Console.ReadLine();
                Console.Write("Введите логин: ");
                Login = Console.ReadLine();
                Console.Write("Введите пароль: ");
                Password = Console.ReadLine();
                user = new User() { Name = Name, LastName = LastName, Email = Email, Login = Login, Password = Password };
                if (repositoryUser.AddUser(user))
                    Console.WriteLine("Пользователь успешно добавлен");
                else
                    Console.WriteLine("Ошибка добавления пользователя");
            }

            return user;
        }
    }
}
