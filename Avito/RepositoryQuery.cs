﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using InterfaceLib;

namespace Avito
{
    public class RepositoryQuery
    {
        private readonly User user;
        private readonly IRepositoryAdvertisement repositoryAdvertisement;
        public bool State { get; set; }
        public RepositoryQuery(User _user, IRepositoryAdvertisement _repositoryAdvertisement)
        {
            State = true;
            user = _user;
            repositoryAdvertisement = _repositoryAdvertisement;
        }
        public string GetResultQuery(string NumberQuery)
        {
            string resultOut = "";

            switch (NumberQuery)
            {
                case "1":
                    Console.Write("\nВведите Id объявления: ");
                    int.TryParse(Console.ReadLine(), out int Id);
                    var repo = repositoryAdvertisement.GetAdvertisementById(Id, user.Login);
                    if (repo != null)
                    {
                        resultOut = repo.ToString();
                    }                        
                    else
                    {
                        resultOut = "По данному Id объявление отсутствует";
                    }                        
                    break;
                case "2":
                    Console.Write("\nВведите текст объявления: ");
                    string TextAdvertisement = Console.ReadLine();
                    Console.Write("Введите стоимость: ");
                    int.TryParse(Console.ReadLine(), out int Cost);
                    var advertisement = new Advertisement() { Text = TextAdvertisement, Cost = Cost, IdUser = user.Id };
                    if (repositoryAdvertisement.AddAdvertisement(advertisement))
                    {
                        resultOut = "Объявление успешно добавлено";
                    }                        
                    else
                    {
                        resultOut = "Ошибка добавления объявления";
                    }                        
                    break;
                case "3":
                    Console.Write("\nВведите Id объявления: ");
                    int.TryParse(Console.ReadLine(), out Id);
                    Console.Write("\nВведите текст объявления: ");
                    TextAdvertisement = Console.ReadLine();
                    Console.Write("Введите стоимость: ");
                    int.TryParse(Console.ReadLine(), out Cost);
                    advertisement = new Advertisement() { Id = Id, Text = TextAdvertisement, Cost = Cost, IdUser = user.Id };
                    if (repositoryAdvertisement.EditAdvertisement(advertisement))
                    {
                        resultOut = "Объявление успешно изменено";
                    }                        
                    else
                    {
                        resultOut = "Ошибка изменения объявления";
                    }                        
                    break;
                case "4":
                    Console.Write("\nВведите Id объявления: ");
                    int.TryParse(Console.ReadLine(), out Id);
                    if (repositoryAdvertisement.DeleteAdvertisement(Id))
                    {
                        resultOut = "Объявление успешно удалено";
                    }                        
                    else
                    {
                        resultOut = "Ошибка удаления объявления";
                    }                        
                    break;
                case "5":
                    var queryAdvertisementActive = repositoryAdvertisement.GetAllActiveAdvertisement();
                    if (queryAdvertisementActive.Count != 0)
                    {
                        resultOut = string.Join('\n', queryAdvertisementActive.Select(s=>s.Text));                        
                    }
                    else
                    {
                        resultOut = "Объявления отсутствуют";
                    }                        
                    break;
                case "6":
                    Console.Write("Введите сумму: ");
                    int.TryParse(Console.ReadLine(), out Cost);
                    var queryAdvertisementCost = repositoryAdvertisement.GetAdvertisementLessCost(Cost);
                    if (queryAdvertisementCost.Count != 0)
                    {
                        resultOut = string.Join('\n', queryAdvertisementCost.Select(s => s.ToString()));                       
                    }
                    else
                    {
                        resultOut = "Объявления отсутствуют";
                    }                        
                    break;
                case "7":
                    Console.Write("Введите дату: ");
                    DateTime.TryParse(Console.ReadLine(), out DateTime dt);
                    var queryAdvertisementAfterDate = repositoryAdvertisement.GetAdvertisementAddAfterDate(dt);
                    if (queryAdvertisementAfterDate.Count != 0)
                    {
                        resultOut = string.Join('\n', queryAdvertisementAfterDate.Select(s => s.ToString()));                       
                    }
                    else
                    {
                        resultOut = "Объявления отсутствуют";
                    }                        
                    break;
                case "8":
                    var queryStatistica = repositoryAdvertisement.GetStatisticaViewAdvertisement();
                    if (queryStatistica.Count != 0)
                    {
                        resultOut = string.Join('\n', queryStatistica.Select(s => s.ToString()));                        
                    }
                    else
                    {
                        resultOut = "Статистика отсутствует";
                    }                        
                    break;
                case "9":
                    var queryStatisticaDay = repositoryAdvertisement.GetAdvertisementMostViewDay();
                    if (queryStatisticaDay.Count != 0)
                    {
                        resultOut = string.Join('\n', queryStatisticaDay.Select(s => s.ToString()));                        
                    }
                    else
                    {
                        resultOut = "Статистика отсутствует";
                    }                        
                    break;
                case "q":
                    resultOut = "Выход из приложения";
                    State = false;
                    break;
            }

            return resultOut;
        }
    }
}
