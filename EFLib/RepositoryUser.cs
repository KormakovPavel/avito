﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using InterfaceLib;

namespace EFLib
{
    public class RepositoryUser : DbContext, IRepositoryUser
    {
        private readonly string connectionString;
        private DbSet<User> Users { get; set; }
        public RepositoryUser()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            connectionString = config.GetConnectionString("DefaultConnection");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(connectionString);                
            }
        }
        
        public User GetUser(string Login, string Password)
        {
            return Users.FirstOrDefault(w => w.Login == Login && w.Password == Password);            
        }

        public bool AddUser(User user)
        {
            Users.Add(user);
            SaveChanges();
            return true;
        }

        public bool EditUser(User user)
        {
            Users.Update(user);
            SaveChanges();
            return true;
        }

        public bool DeleteUser(int Id)
        {
            var user = Users.FirstOrDefault(p => p.Id == Id);
            Users.Remove(user);
            SaveChanges();
            return true;
        }
    }
}
