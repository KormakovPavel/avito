﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using InterfaceLib;

namespace EFLib
{
    public class RepositoryAdvertisement : DbContext, IRepositoryAdvertisement
    {
        private readonly string connectionString;
        private DbSet<Advertisement> Advertisements { get; set; }
        private DbSet<History> Histories { get; set; }
        private DbSet<Status> Statusy { get; set; }
        private DbSet<User> Users { get; set; }
        private DbSet<Statistica> Statistics { get; set; }
        public RepositoryAdvertisement()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            connectionString = config.GetConnectionString("DefaultConnection");            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(connectionString);                
            }
        }

        public List<Advertisement> GetAdvertisementList()
        {            
            return Advertisements.ToList();
        }

        public Advertisement GetAdvertisementById(int Id, string Login)
        {
            var advertisement = Advertisements.FirstOrDefault(w => w.Id == Id);
            if (advertisement != null)
            {
                var user = Users.FirstOrDefault(f => f.Login == Login);
                if (advertisement.IdUser != user.Id)
                {
                    var statistic = new Statistica() { CountLook = 1, IdAdvertisement = Id, Date_Time = DateTime.Now };
                    Statistics.Add(statistic);
                    SaveChanges();
                }
            }

            return advertisement;
        }

        public bool AddAdvertisement(Advertisement advertisement)
        {
            Advertisements.Add(advertisement);
            SaveChanges();
            var advertisementId = Advertisements.FirstOrDefault(f => f.IdUser == advertisement.IdUser && f.Text == advertisement.Text).Id;

            var history = new History() { Comment = "Объявление добавлено", IdAdvertisement = advertisementId, Date_Time=DateTime.Now };
            Histories.Add(history);

            var status = new Status() { Value = "Активно", IdAdvertisement = advertisementId };
            Statusy.Add(status);

            SaveChanges();
            return true;
        }

        public bool EditAdvertisement(Advertisement advertisement)
        {
            var history = new History() { Comment = "Объявление изменено", IdAdvertisement = advertisement.Id, Date_Time = DateTime.Now };
            Advertisements.Update(advertisement);
            Histories.Add(history);
            SaveChanges();
            return true;
        }

        public bool DeleteAdvertisement(int Id)
        {
            var advertisement = Advertisements.FirstOrDefault(w => w.Id == Id);
            if (advertisement != null)
            {
                var history = new History() { Comment = "Объявление удалено", IdAdvertisement = Id, Date_Time = DateTime.Now };
                Histories.Add(history);

                var status = Statusy.FirstOrDefault(f => f.IdAdvertisement == Id);
                status.Value = "Удалено";                
                Statusy.Update(status);

                SaveChanges();
                return true;
            }
            else
                return false;
        }

        public List<Advertisement> GetAllActiveAdvertisement()
        {
            return GetAdvertisementList().Join(Statusy.ToList().Where(w => w.Value == "Активно"), u => u.Id, a => a.IdAdvertisement,
                                                              (u, a) => new Advertisement { Text = u.ToString() }).ToList();
        }

        public List<Advertisement> GetAdvertisementLessCost(int Cost)
        {
            return GetAdvertisementList().Where(w => w.Cost < Cost).ToList();
        }

        public List<Advertisement> GetAdvertisementAddAfterDate(DateTime dt)
        {
            return GetAdvertisementList().Join(Histories.ToList().Where(w => w.Comment == "Объявление добавлено"
                                                              && DateTime.Compare(w.Date_Time, dt) > 0), u => u.Id, a => a.IdAdvertisement, (u, a) => new Advertisement { Text = u.ToString() }).ToList();            
        }

        public List<StatisticaView> GetStatisticaViewAdvertisement()
        {
            return GetAdvertisementList().GroupJoin(Statistics.ToList(), u => u.Id, a => a.IdAdvertisement,
                                                  (u, a) => new StatisticaView { Name = u.ToString(), CountLook = a.Sum(s => s.CountLook) }).ToList();
            
        }

        public List<StatisticaView> GetAdvertisementMostViewDay()
        {
            return GetAdvertisementList().GroupJoin(Statistics.ToList().Where(w => DateTime.Compare(w.Date_Time, DateTime.Now.AddDays(-1)) > 0), u => u.Id, a => a.IdAdvertisement,
                                                 (u, a) => new StatisticaView  { Name = u.ToString(), CountLook = a.Sum(s => s.CountLook) }).Where(w => w.CountLook > 1).ToList();           

        }
    }
}
