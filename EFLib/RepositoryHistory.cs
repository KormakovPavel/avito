﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using InterfaceLib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace EFLib
{
    public class RepositoryHistory : DbContext, IRepository<History>
    {
        private readonly string connectionString;
        private DbSet<History> Histories { get; set; }

        public RepositoryHistory()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            connectionString = config.GetConnectionString("DefaultConnection");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(connectionString);                
            }
        }

        public List<History> GetList()
        {
            return Histories.ToList();
        }

    }
}
