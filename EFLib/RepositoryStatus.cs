﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using InterfaceLib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace EFLib
{
    public class RepositoryStatus : DbContext, IRepository<Status>
    {
        private readonly string connectionString;
        private DbSet<Status> Statusy { get; set; }

        public RepositoryStatus()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            connectionString = config.GetConnectionString("DefaultConnection");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(connectionString);               
            }
        }

        public List<Status> GetList()
        {
            return Statusy.ToList();
        }

    }
}
