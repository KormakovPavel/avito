﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using InterfaceLib;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace ADOLib
{
    public class RepositoryHistory : IRepository<History>, IDisposable
    {
        private readonly SqlConnection connection;        

        public RepositoryHistory()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            connection = new SqlConnection(config.GetConnectionString("DefaultConnection"));
            connection.Open();
        }

        public void Dispose()
        {
            connection.Close();
        }

        public List<History> GetList()
        {
            var command = new SqlCommand($"SELECT * FROM dbo.Histories", connection);
            var reader = command.ExecuteReader();

            var HistoryList = new List<History>();
            while (reader.Read())
            {
                HistoryList.Add(new History()
                {
                    Id = Convert.ToInt32(reader[0].ToString()),
                    Date_Time = Convert.ToDateTime(reader[1].ToString()),
                    Comment = reader[2].ToString(),
                    IdAdvertisement = Convert.ToInt32(reader[3].ToString())                    
                });
            }
            reader.Close();

            return HistoryList;
        }

    }
}
