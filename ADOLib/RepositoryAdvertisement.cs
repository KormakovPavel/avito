﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using InterfaceLib;

namespace ADOLib
{
    public class RepositoryAdvertisement : IRepositoryAdvertisement, IDisposable
    {
        private readonly SqlConnection connection;
        private readonly IRepository<Statistica> repoStatistica;
        private readonly IRepository<Status> repoStatus;
        private readonly IRepository<History> repoHistory;
        public RepositoryAdvertisement()
        {
            repoStatistica = new RepositoryStatistica();
            repoStatus = new RepositoryStatus();
            repoHistory = new RepositoryHistory();

            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();            
            connection = new SqlConnection(config.GetConnectionString("DefaultConnection"));
            connection.Open();
        }

        public List<Advertisement> GetAdvertisementList()
        {
            var command = new SqlCommand($"SELECT * FROM dbo.Advertisements", connection);
            var reader = command.ExecuteReader();

            var AdvertisementList = new List<Advertisement>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    AdvertisementList.Add(new Advertisement()
                    {
                        Id = Convert.ToInt32(reader[0].ToString()),
                        Text = reader[1].ToString(),
                        Cost = Convert.ToInt32(reader[2].ToString()),
                        IdUser = Convert.ToInt32(reader[3].ToString())
                    });
                }                
            }
            reader.Close();

            return AdvertisementList;
        }

        public Advertisement GetAdvertisementById(int Id, string Login)
        {
            Advertisement advertisement = null;
            var command = new SqlCommand($"SELECT * FROM dbo.Advertisements WHERE Id={Id}", connection);
            var reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                advertisement = new Advertisement()
                {
                    Id = Convert.ToInt32(reader[0].ToString()),
                    Text = reader[1].ToString(),
                    Cost = Convert.ToInt32(reader[2].ToString()),
                    IdUser = Convert.ToInt32(reader[3].ToString())
                };                
            }
            reader.Close();

            if (advertisement != null)
            {               
                command = new SqlCommand($"SELECT Id FROM dbo.Users WHERE Login='{Login}'", connection);
                reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    if (advertisement.IdUser != Convert.ToInt32(reader[0].ToString()))
                    {
                        reader.Close();
                        command = new SqlCommand($"INSERT INTO [dbo].[Statistics](CountLook,IdAdvertisement,Date_Time) VALUES (1,{Id},'{DateTime.Now}')", connection);
                        command.ExecuteNonQuery();
                    }
                    else
                        reader.Close();
                } 
            }

            return advertisement;
        }

        public bool AddAdvertisement(Advertisement advertisement)
        {
            var command = new SqlCommand($"INSERT INTO dbo.Advertisements(Text,IdUser,Cost) VALUES ('{advertisement.Text}',{advertisement.IdUser},'{advertisement.Cost}')", connection);
            command.ExecuteNonQuery();

            command = new SqlCommand($"SELECT Id FROM dbo.Advertisements WHERE IdUser={advertisement.IdUser} AND Text='{advertisement.Text}'", connection);
            var reader = command.ExecuteReader();           
            if (reader.HasRows)
            {
                reader.Read();
                var advertisementId = Convert.ToInt32(reader[0].ToString());
                reader.Close();

                command = new SqlCommand($"INSERT INTO dbo.Histories(Date_Time,Comment,IdAdvertisement) VALUES ('{DateTime.Now}','Объявление добавлено',{advertisementId})", connection);
                command.ExecuteNonQuery();

                command = new SqlCommand($"INSERT INTO dbo.Statusy(Value,IdAdvertisement) VALUES ('Активно',{advertisementId})", connection);
                command.ExecuteNonQuery();
            } 
            else
                reader.Close();


            return true;
        }

        public bool EditAdvertisement(Advertisement advertisement)
        {
            var command = new SqlCommand($"UPDATE dbo.Advertisements SET Text='{advertisement.Text}',IdUser={advertisement.IdUser},Cost={advertisement.Cost}" +
                                         $" WHERE Id={advertisement.Id}", connection);
            command.ExecuteNonQuery();

            command = new SqlCommand($"INSERT INTO dbo.Histories(Date_Time,Comment,IdAdvertisement) VALUES ('{DateTime.Now}','Объявление изменено',{advertisement.Id})", connection);
            command.ExecuteNonQuery();

            return true;
        }

        public bool DeleteAdvertisement(int Id)
        {
            var command = new SqlCommand($"SELECT * FROM dbo.Advertisements WHERE Id={Id}", connection);
            var reader = command.ExecuteReader();
            reader.Read();           
            if (reader.HasRows)
            {
                reader.Close();
                command = new SqlCommand($"INSERT INTO dbo.Histories(Date_Time,Comment,IdAdvertisement) VALUES ('{DateTime.Now}','Объявление удалено',{Id})", connection);
                command.ExecuteNonQuery();

                command = new SqlCommand($"UPDATE dbo.Statusy SET Value='Удалено' WHERE IdAdvertisement={Id}", connection);
                command.ExecuteNonQuery();
                
                return true;
            }
            else
            {
                reader.Close();
                return false;
            }
                
        }

        public List<Advertisement> GetAllActiveAdvertisement()
        {
            return GetAdvertisementList().Join(repoStatus.GetList().Where(w => w.Value == "Активно"), u => u.Id, a => a.IdAdvertisement,
                                                              (u, a) => new Advertisement { Text = u.ToString() }).ToList();
        }

        public List<Advertisement> GetAdvertisementLessCost(int Cost)
        {
            return GetAdvertisementList().Where(w => w.Cost < Cost).ToList();
        }

        public List<Advertisement> GetAdvertisementAddAfterDate(DateTime dt)
        {
            return GetAdvertisementList().Join(repoHistory.GetList().Where(w => w.Comment == "Объявление добавлено"
                                                              && DateTime.Compare(w.Date_Time, dt) > 0), u => u.Id, a => a.IdAdvertisement, (u, a) => new Advertisement { Text = u.ToString() }).ToList();
        }

        public List<StatisticaView> GetStatisticaViewAdvertisement()
        {
            return GetAdvertisementList().GroupJoin(repoStatistica.GetList(), u => u.Id, a => a.IdAdvertisement,
                                                  (u, a) => new StatisticaView { Name = u.ToString(), CountLook = a.Sum(s => s.CountLook) }).ToList();

        }

        public List<StatisticaView> GetAdvertisementMostViewDay()
        {
            return GetAdvertisementList().GroupJoin(repoStatistica.GetList().Where(w => DateTime.Compare(w.Date_Time, DateTime.Now.AddDays(-1)) > 0), u => u.Id, a => a.IdAdvertisement,
                                                 (u, a) => new StatisticaView { Name = u.ToString(), CountLook = a.Sum(s => s.CountLook) }).Where(w => w.CountLook > 1).ToList();

        }

        public void Dispose()
        {
            connection.Close();
        }
    }
}
