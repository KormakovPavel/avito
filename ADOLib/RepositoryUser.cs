﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using InterfaceLib;

namespace ADOLib
{
    public class RepositoryUser : IRepositoryUser, IDisposable
    {        
        private readonly SqlConnection connection;
        public RepositoryUser()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            connection = new SqlConnection(config.GetConnectionString("DefaultConnection"));
            connection.Open();
        }       
        
        public User GetUser(string Login, string Password)
        {
            User user=null;
            var command = new SqlCommand($"SELECT * FROM dbo.Users WHERE Login='{Login}' AND Password='{Password}'", connection);
            var reader = command.ExecuteReader();
            reader.Read();
            if(reader.HasRows)
            {
                user = new User()
                {
                    Id = Convert.ToInt32(reader[0].ToString()),
                    Name = reader[1].ToString(),
                    LastName = reader[2].ToString(),
                    Email = reader[3].ToString(),
                    Login = reader[4].ToString(),
                    Password = reader[5].ToString()
                };            
            }             
            reader.Close();

            return user;
        }

        public bool AddUser(User user)
        {
            var command = new SqlCommand($"INSERT INTO dbo.Users(Name,LastName,Email,Login,Password) VALUES ('{user.Name}','{user.LastName}','{user.Email}'," +
                                         $"'{user.Login}','{user.Password}')", connection);
            command.ExecuteNonQuery();
            return true;
        }

        public bool EditUser(User user)
        {
            var command = new SqlCommand($"UPDATE dbo.Users SET Name='{user.Name}',LastName='{user.LastName}',Email='{user.Email}',Login='{user.Login}',Password='{user.Password}') " +
                                         $"WHERE Id={user.Id}", connection);
            command.ExecuteNonQuery();
            return true;
        }

        public bool DeleteUser(int Id)
        {
            var command = new SqlCommand($"DELETE FROM dbo.Users WHERE Id={Id}", connection);
            command.ExecuteNonQuery();
            return true;
        }

        public void Dispose()
        {
            connection.Close();
        }
    }
}
