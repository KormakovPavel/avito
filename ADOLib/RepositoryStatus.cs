﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using InterfaceLib;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace ADOLib
{
    public class RepositoryStatus : IRepository<Status>, IDisposable
    {
        private readonly SqlConnection connection;        

        public RepositoryStatus()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            connection = new SqlConnection(config.GetConnectionString("DefaultConnection"));
            connection.Open();
        }

        public void Dispose()
        {
            connection.Close();
        }

        public List<Status> GetList()
        {
            var command = new SqlCommand($"SELECT * FROM dbo.Statusy", connection);
            var reader = command.ExecuteReader();

            var StatusList = new List<Status>();
            while (reader.Read())
            {
                StatusList.Add(new Status()
                {
                    Id = Convert.ToInt32(reader[0].ToString()),                   
                    Value = reader[1].ToString(),
                    IdAdvertisement = Convert.ToInt32(reader[2].ToString())                    
                });
            }
            reader.Close();

            return StatusList;
        }

    }
}
