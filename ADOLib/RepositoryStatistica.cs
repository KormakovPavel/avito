﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using InterfaceLib;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace ADOLib
{
    public class RepositoryStatistica : IRepository<Statistica>, IDisposable
    {
        private readonly SqlConnection connection;        

        public RepositoryStatistica()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            connection = new SqlConnection(config.GetConnectionString("DefaultConnection"));
            connection.Open();
        }

        public void Dispose()
        {
            connection.Close();
        }

        public List<Statistica> GetList()
        {
            var command = new SqlCommand($"SELECT * FROM [dbo].[Statistics]", connection);
            var reader = command.ExecuteReader();

            var StatisticaList = new List<Statistica>();
            while (reader.Read())
            {
                StatisticaList.Add(new Statistica()
                {
                    Id = Convert.ToInt32(reader[0].ToString()),
                    CountLook = Convert.ToInt32(reader[1].ToString()),
                    IdAdvertisement = Convert.ToInt32(reader[2].ToString()),
                    Date_Time = Convert.ToDateTime(reader[3].ToString())
                });
            }
            reader.Close();

            return StatisticaList;
        }

    }
}
