﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceLib
{
    public class History
    {
        public int Id { get; set; }
        public DateTime Date_Time { get; set; }
        public string Comment { get; set; }
        public int IdAdvertisement { get; set; }       
    }
}
