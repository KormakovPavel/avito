﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceLib
{
    public class StatisticaView
    {
        public string Name { get; set; }
        public int CountLook { get; set; }
        public override string ToString()
        {
            return $"Объявление: {Name}, количество просмотров:{CountLook}";
        }
    }
}
