﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceLib
{
    public class Status
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int IdAdvertisement { get; set; }      
    }
}
