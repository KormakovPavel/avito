﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceLib
{
    public class Advertisement
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int Cost { get; set; }        
        public int IdUser { get; set; }

        public override string ToString()
        {
            return $"Текст: {Text}, Стоимость:{Cost}";
        }
    }
}
