﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceLib
{
    public interface IRepositoryUser
    {
        User GetUser(string Login, string Password);
        bool AddUser(User user);
        bool EditUser(User user);
        bool DeleteUser(int Id);
    }
}
