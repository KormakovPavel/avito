﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceLib
{
    public interface IRepositoryAdvertisement
    {
        List<Advertisement> GetAdvertisementList();
        Advertisement GetAdvertisementById(int Id,string Login);
        bool AddAdvertisement(Advertisement advertisement);
        bool EditAdvertisement(Advertisement advertisement);
        bool DeleteAdvertisement(int Id);
        List<Advertisement> GetAllActiveAdvertisement();
        List<Advertisement> GetAdvertisementLessCost(int Cost);
        List<Advertisement> GetAdvertisementAddAfterDate(DateTime dt);
        List<StatisticaView> GetStatisticaViewAdvertisement();
        List<StatisticaView> GetAdvertisementMostViewDay();
    }
}
