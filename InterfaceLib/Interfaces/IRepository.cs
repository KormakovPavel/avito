﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceLib
{
    public interface IRepository<T>
    {
        List<T> GetList();       
    }
}
